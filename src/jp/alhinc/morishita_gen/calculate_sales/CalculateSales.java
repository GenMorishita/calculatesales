package jp.alhinc.morishita_gen.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {


	public static void main(String[] args) {

		HashMap<String, String> branchMap = new HashMap<String, String>();

		HashMap<String, Long> branchSalesMap = new HashMap<String, Long>();

		ArrayList<String> rcdList = new ArrayList<String>();

		ArrayList<String> rcdFileNameList = new ArrayList<String>();




		//コマンドライン引数の数をチェック
		if(args.length != 1){

			System.out.println("予期せぬエラーが発生しました");
			return;
		}


		//読み込みメソッド呼び出し

		if(!readFile(args[0], "branch.lst", branchMap, branchSalesMap, "支店", "^[0-9]{3}$")){

			return;
		}


		//指定したディレクトリから拡張子が.rcdでファイル名が数字8桁のファイルを探す


		//ディレクトリ内のファイル名を取得

		File dir = new File(args[0]);

		File[] files = dir.listFiles();

		for(int i = 0; i < files.length; i++){

			String name = files[i].getName();

			if(files[i].isFile() && name.matches("^[0-9]{8}.rcd$")){

				rcdList.add(name);
			}
		}


		//数字8桁.rcdのファイル名のみ取り出す

		for(int i = 0; i < rcdList.size(); i++){

			rcdFileNameList.add(rcdList.get(i).substring(0, 8));		//数字8桁+.rcdの全てのファイル名
		}


		//ファイル名が連番になっているかチェック

		for(int i = 0; i < rcdFileNameList.size() - 1; i++){

			int currentFileName = Integer.parseInt(rcdFileNameList.get(i));

			int nextFileName = Integer.parseInt(rcdFileNameList.get(i + 1));

			if(currentFileName != nextFileName - 1){

				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}


		//売上ファイルの数だけテキストを読み込む

		for(int i = 0; i < rcdList.size(); i++){

			ArrayList<String> branchCodeAndSalesList = new ArrayList<String>();

			BufferedReader br = null;

			try{
				File file = new File(args[0], rcdList.get(i));
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String branchCodeAndSales = null;

				while((branchCodeAndSales = br.readLine()) != null){

					branchCodeAndSalesList.add(branchCodeAndSales);
				}


				//売上ファイルのフォーマットをチェック

				if(branchCodeAndSalesList.size() != 2){

					System.out.println(rcdList.get(i) + "のフォーマットが不正です");
					return;
				}


				//売上金額のフォーマットをチェック

				if(!branchCodeAndSalesList.get(1).matches("^[0-9]{1,}$")){

					System.out.println("予期せぬエラーが発生しました");
					return;
				}


				//読み込んだ支店コードが存在するかチェック

				if(!branchMap.containsKey(branchCodeAndSalesList.get(0))){

					System.out.println(rcdList.get(i) + "の支店コードが不正です");
					return;
				}


				//読み込んだ売上額を該当する支店の合計金額に加算

				long readBranchSales = Long.parseLong(branchCodeAndSalesList.get(1));	//ループで読み込んだ新しい値

				long currentBranchSales = branchSalesMap.get(branchCodeAndSalesList.get(0));	//ループの外にある元の値（初期値 = 0）

				currentBranchSales += readBranchSales;		//元の値に新しい値を足す


				//合計金額が10桁を超えていないかチェック

				if(currentBranchSales > 9999999999L){

					System.out.println("合計金額が10桁を超えました");
					return;
				}


				//合計金額の値を更新

				branchSalesMap.put(branchCodeAndSalesList.get(0), currentBranchSales);

			}catch(IOException e){

				System.out.println("予期せぬエラーが発生しました");
				return;

			}finally{
				if(br != null){
					try{

						br.close();

					}catch(IOException e){

						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}


		//書き出しメソッド呼び出し

		if(!writeFile(args[0], "branch.out", branchMap, branchSalesMap)){

			return;
		}
	}




	//読み込みメソッド

	static boolean readFile(String path, String fileName, Map<String, String> nameMap, Map<String, Long> salesMap, String branchName, String branchCode){


		//ファイルを探す

		BufferedReader br = null;
		try{
			File file = new File(path, fileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);


			//ファイルの中身を1行ごと読み込む

			String readFile = null;

			while((readFile = br.readLine()) != null){


				//読み込んだ行を","で分ける

				String[] splitReadFile = readFile.split(",");


				//ファイルのフォーマットをチェック

				if(splitReadFile.length != 2){

					System.out.println(branchName + "定義ファイルのフォーマットが不正です");
					return false;
				}

				if(!splitReadFile[0].matches(branchCode)){

					System.out.println(branchName + "定義ファイルのフォーマットが不正です");
					return false;
				}


				//支店コードをkey、支店名をvalueにしてmapにしまう

				nameMap.put(splitReadFile[0], splitReadFile[1]);

				salesMap.put(splitReadFile[0], (long) 0);
			}

		}catch(IOException e){

			System.out.println(branchName + "定義ファイルが存在しません");
			return false;

		}finally{
			if(br != null){
				try{

					br.close();

				}catch(IOException e){

					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}



	//書き出しメソッド

	static boolean writeFile(String path, String fileName, Map<String, String> nameMap, Map<String, Long> salesMap){


		//指定したディレクトリにファイルを作成

		BufferedWriter bw = null;
		try{
			File file = new File(path, fileName);

			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);


			//データを出力

			for(Map.Entry<String, Long> writingData : salesMap.entrySet()){

				bw.write(writingData.getKey() + "," + nameMap.get(writingData.getKey()) + "," + writingData.getValue());
				bw.newLine();
			}

		}catch(IOException e){

			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally{
			if(bw != null){
				try{

					bw.close();

				}catch(IOException e){

					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}